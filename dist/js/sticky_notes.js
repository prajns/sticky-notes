// Zdarznie które zapewnia, że skrypt zacznie się wykonywać po załadowaniu strony
$(document).ready(() => {

    // Globalna zmienna za pomocą, której definiowany jest id użytkownika potrzebne do operacji w bazie
    var uid = -1;

    // Globalna zmienna do timeoutu w evencie resize
    let doOnResize;

    // Funkcja która czyści pola w modalach oraz błędy, które pojawiają się przy niepoprawnych danych logowania
    $.fn.clearModalInputs = () => {
        $("#accountModal input").val('');
        $("#accountModal .alert-danger").hide();
    }

    // Modale - to wyskakujące okienka, u nas całe logowani i rejestracj na nich stoi
    // gdybyście ich szukali są w pliku sticky_notes.html, mimo że tam są domyślnie są ukryte 
    // taka funkcjonalność bootstrapa, więcej na ten temat: https://getbootstrap.com/docs/4.3/components/modal/

    // Funkcja która pozwala zalogować się użytkownikowi
    // l -> login | p -> password
    $.fn.login = (l, p) => {
        if (!l || !p) {
            // Jeśli nie chociaż jedno pole jest puste to pokazuje błąd w specjalnym elemencie pod formularzem
            $('#loginalert').text('Missing data!').show();
        } else {
            // Wysyłanie żądania do serwera na wskazany adres, przekazujemy dane logowania czyli login i hasło
            $.ajax({
                type: 'POST',
                url: 'http://127.0.0.1:3000/api/user_login',
                data: `l=${l}&p=${p}`,
                success: (res) => {
                    if (res.length) {
                        // Przy pozytywnej odpowiedzi, czyli zwróconym rekordem z bazy:
                        // Czyścimy wszystkie pola funkcją, która jest zdefiniowana u samej góry
                        $.fn.clearModalInputs();
                        // chowamy modala z formularzem logowania
                        $('#loginbox').hide(); 
                        // pokazujemy modala z panelem użytkownika
                        $('#userbox').show();
                        // wyświetlamy nazwę użytkownika w panelu użytkownika 
                        // mamy do niej dostęp bo dostaliśmy ją w odpowiedzi (res) od serwera
                        $('#user_login').text(res[0].login);
                        // zmieniamy zawartość przycisku logowania
                        $("#btn--user").html("<i class='fas fa-user'></i> Account");
                        // zmieniamy zawartość przycisku dodawnia notatki
                        $("#btn--add").html("<i class='fas fa-plus'></i> Add note");
                        // zmieniamy działanie dodawania notatek z localstorage na baze
                        $("#btn--add").toggleClass("ls--usage");
                        // Czyścimy DOM z notatek localstorage
                        $("section").empty();
                        // przypisujemy id zalogowanego użytkownika do zmiennej globalnej
                        uid = res[0].id_user;
                        // Wysyłamy kolejne żądanie tym razem, aby pobrać wszystkie notatki należące do użytkownika
                        $.ajax({
                            type: 'POST',
                            url: 'http://127.0.0.1:3000/api/notes',
                            data: `uid=${uid}`,
                            success: (res) => {
                                //przechodzimy po wszystkich otrzymanych rekordach (otrzymanych notatkach)
                                $.each(res, function(index, value) {
                                    //wykorzystujemy funkcję renderującą notatkę aby stworzyć fizyczne obiekty na stronie
                                    const note = $.fn.renderNote(res[index].id_note, res[index].content);
                                    //nadajemy style każdej notatce
                                    $(note).css({
                                        "background-color": res[index].color, 
                                        "x": res[index].x + "px", 
                                        "y": res[index].y + "px", 
                                        "width": res[index].width + "px", 
                                        "height": res[index].height + "px", 
                                        "left": res[index].p_left + "px", 
                                        "top": res[index].p_top + "px", 
                                        "bottom": res[index].p_bottom + "px", 
                                        "right": res[index].p_right  + "px"});
                                });
                            }
                        });
                    } else {
                        // Dupa, coś poszło źle nie znaleziono użytkownika o takich danych logowania
                        $('#loginalert').text('Wrong data!').show();
                    }
                },
            });
        }
    }

    // Zdarzenie wywoływane pod kliknięciu logowania w menu głównym
    $('#btn-login').click(() => {
        // Przechwytujemy wartości wpisane do poszczególnych pól formularza
        const l = $('#login_login').val();
        const p = $('#login_password').val();

        // Przekazujemy te wartości do funkcji logowania
        $.fn.login(l, p);
    });

    // Zdarzenie wywoływane pod kliknięciu przycisku wylogowania w panelu użytkownika
    $('#btn-logout').click(() => {
        // Chowamy modal z panelem użytkownika
        $('#userbox').hide();
        // Wyświetlamy modal z formularzem logowania
        $('#loginbox').show();
        // Czyścimy wszystkie pola w formularzach
        $.fn.clearModalInputs();
        // zmieniamy zawartość przycisku logowania
        $("#btn--user").html("<i class='fas fa-sign-in-alt'></i> Sign in");
        // zmieniamy zawartość przycisku dodawnia notatki
        $("#btn--add").html("<i class='fas fa-plus'></i> Add local note");
        // Ustawiamy klasę blokującą klik przycisku z dodawaniem notatek
        $("#btn--add").toggleClass("ls--usage");
        // Czyścimy informacje o użytkowniku ze zmiennej globalnej
        uid = -1;
        // Usuwamy notatki z widoku na stronie (NIE Z BAZY!)
        $("section").empty();
        //Wyświetlamy notateki z localstorage
        $.fn.displayLocalstorageNotes();
    });

    // Zdarzenie wywoływane pod kliknięciu przycisku rejestracji w modalu logowania
    $('#btn-signup').click(() => {
        // Przechwytujemy wartości wpisane do poszczególnych pól formularza i umieszczamy je do zmiennych
        // Możecie używać var, let oraz const - najbezpieczniej jest używać var jak nie wiecie co i jak
        // let to taki nowszy var jednak różni się zasięgiem - zmienna w tej funkcji będzie widoczna tylko w tej funkcji
        // const to stała, która ma taki sam zasięg jak let, a różni się tym że nie można zmienić jej wartości
        // pr -> powtórzone hasło (pełna profeska można powiedzieć - nie :D)
        const l = $('#register_login').val();
        const p = $('#register_password').val();
        const pr = $('#register_password_re').val();

        if (!l || !p || !pr){
            // Podobnie jak wcześniej sprawdzamy czy wszystkie pola zostały uzupełnione,
            // jeśli nie to wyrzucamy tekst do elementu z błędami
            $('#signupalert').text('Missing data!').show();
        } else {
            if(p === pr) {
                // Jeśli hasła się zgadzają to wysyłamy żądanie na serwer
                $.ajax({
                    type: 'POST',
                    url: 'http://127.0.0.1:3000/api/user_register',
                    data: `l=${l}&p=${p}`,
                    success: () => {
                        // Kiedy udało się wstawić rekord do bazy (czyli zarejestrowano użytkownika pomyślnie)
                        // Schowaj modal rejestracji
                        $('#signupbox').hide();
                        // Wywołaj funkcję logowania automatycznie z danymi którymi się rejestrowaliśmy
                        // Możecie prześledzić jej wynik u góry (wykona te same czynności)
                        $.fn.login(l, p);
                    }
                });
            } else {
                // Błąd kiedy hasła się różnią
                $('#signupalert').text('Passwords are not same!').show();
            }
        }
    });

    // Uniwersalna funkcja do tworzenia notatek
    $.fn.renderNote = function(id, content, index = -1){

        // Tworzenie pustego diva
        const note = document.createElement("div");
        // Nadanie mu id, przypisanie do rodzica "section" oraz nadanie mu własności draggable i resizable z biblioteki jQuery UI
        $(note).addClass("sticker").attr("id", "n" + id).appendTo($("section")).draggable({ 
            containment: "parent", 
            stack: ".sticker", 
            cancel: ".note--basic__body", 
            stop: function() {
                // getBoundingClientRect() -> to taka fajna funkcja która zwraca obiekt z wszystkim co nam potrzeba jeśli chodzi o wymiary i położenie notatki
                let rect = note.getBoundingClientRect();

                if( $("#btn--add").hasClass("ls--usage") ){
                    // Aktualizuj Rect w notesArray
                    notesArray[index].rect = rect;

                    // Aktualizuj localstorage
                    localStorage.setItem("notes", JSON.stringify(notesArray));
                } else {
                    // Wysłanie żądania z updatem notatki po skończeniu poruszania notatką
                    $.ajax({
                        type: 'PUT',
                        url: 'http://127.0.0.1:3000/api/update_note_rect',
                        data: `id_note=${id}&x=${rect.x}&y=${rect.y}&width=${rect.width}&height=${rect.height}&p_top=${rect.top}&p_right=${rect.right}&p_bottom=${rect.bottom}&p_left=${rect.left}`,
                        success: () => { }
                    });
                }
            }
        }).resizable( {
            minHeight: 240, 
            minWidth: 200,
            containment: "parent", 
            stop: function() {
                let rect = note.getBoundingClientRect();

                if( $("#btn--add").hasClass("ls--usage") ){
                    // Aktualizuj Rect w notesArray
                    notesArray[index].rect = rect;

                    // Aktualizuj localstorage
                    localStorage.setItem("notes", JSON.stringify(notesArray));
                } else {
                    // Wysłanie żądania z updatem notatki po skończeniu manipulacją rozmiarem notatki
                    $.ajax({
                        type: 'PUT',
                        url: 'http://127.0.0.1:3000/api/update_note_rect',
                        data: `id_note=${id}&x=${rect.x}&y=${rect.y}&width=${rect.width}&height=${rect.height}&p_top=${rect.top}&p_right=${rect.right}&p_bottom=${rect.bottom}&p_left=${rect.left}`,
                        success: () => { }
                    });
                }
            }
        });

        // Szablon paska narzędzi do notatki - specjalna struktura quill.js
        const noteHeader = document.createElement("div");
        $(noteHeader).addClass("bar").attr("id", "nht" + id).html(`
            <span class='ql-formats'>        
                <button class="ql-bold"></button>
                <button class="ql-italic"></button>
                <button class="ql-underline"></button>
                <button class="ql-strike"></button>
                <button class="ql-list" value="ordered"></button>
                <button class="ql-list" value="bullet"></button>
                <button class="ql-image"></button>
                <select class='ql-color' data-toggle='tooltip' data-placement='top' title='Kolor tekstu'></select>
                <select class='ql-background' data-toggle='tooltip' data-placement='top' title='Kolor wyróżnienia tekstu' onchange="$(this).closest('.sticker').css('background-color',this.value)"></select>
            </span>
        `).appendTo($(note));


        // Div na dole z usuwaniem notatki
        const noteHeaderClose = document.createElement("button");
        // Nadanie klasy, umieszczenie tekstu oraz przypisanie do rodzica
        $(noteHeaderClose).addClass("ql-close").html("<i class='fas fa-trash'></i>").appendTo($(noteHeader));

        // Przypisanie zdarzenia na kliknięcie Delete w każdej notatce
        $(noteHeaderClose).click(function(){
            // Upewnienie się czy na pewno usunąć notatkę -> zwraca true jeśli potwierdzimy
            // Jeśli nie to nic się nie dzieje
            const res = confirm("Do you want to delete this note?");
            if (res == true) {
                
                if( $("#btn--add").hasClass("ls--usage") ){
                    // Fizyczne usunięcie notatki ze strony (ze struktury DOM)
                    $(this).parent().parent().remove();

                    // Usunięcie notatki z tablicy notatek
                    notesArray.splice(index, 1)

                    // Aktualizacja localstorage
                    localStorage.setItem("notes", JSON.stringify(notesArray));
                } else {
                    // Żądanie usunięcia notatki z bazy, jeśli się uda to usuwamy również fizycznie element ze strony
                    $.ajax({
                        type: 'DELETE',
                        url: 'http://127.0.0.1:3000/api/delete_note',
                        data: `id_note=${id}`,
                        success: () => {
                            // Fizyczne usunięcie notatki ze strony (ze struktury DOM)
                            $(this).parent().parent().remove();
                        }
                    });
                }
            }
        });

        // Body of note - container for Quill rich text editor
        const noteBody = document.createElement("div");
        $(noteBody).addClass("note--basic__body").attr("id", "nb" + id).html(content).appendTo($(note));

        // Inicjalizacja edytora tekstowe we wskazanym elemencie -> po to potrzebne było unikalne id
        const editor = new Quill(`#${$(noteBody).attr("id")}`, {
            modules: {
                // Wskazanie naszego paska z narzędziami jako obiektu toolbar dla quill.js
              toolbar: `#${$(noteHeader).attr("id")}`
            },
            theme: 'snow'
        });

        // Zdarzenie wywoływane za każdym razem kiedy coś się zmieni w notatce
        editor.on('text-change', function() {
            const tmpContent = editor.root.innerHTML;
            const tmpColor = $(note).css("background-color");

            if( $("#btn--add").hasClass("ls--usage") ){
                // Aktualizuj tablicę notatek
                notesArray[index].content = tmpContent;
                notesArray[index].color = tmpColor;

                // Aktualizuj localstorage
                localStorage.setItem("notes", JSON.stringify(notesArray));
            } else {
                // Wysłanie żądania z updatem koloru oraz zawartości notatki
                $.ajax({
                    type: 'PUT',
                    url: 'http://127.0.0.1:3000/api/update_note_content',
                    data: `content=${tmpContent}&color=${tmpColor}&id_note=${id}`,
                    success: () => {}
                });
            }
        });
        
        return note;
    }

    // Pobierz tablicę z notatkami lokalnymi z localstorage
    let notesArray = JSON.parse(localStorage.getItem("notes") || "[]");

    // Funkcja do wyświetlania notatek zapisanych w localstorage
    $.fn.displayLocalstorageNotes = function(){
        // Dla każdej zapisanej w localstorage notatki wywołaj funkcje renderującą notatę oraz nadaj jej odpowiednie style
        $.each(notesArray, function(index, value) {
            const note = $.fn.renderNote(value.id, value.content, index);
            $(note).css({ "background-color": notesArray[index].color, "left": notesArray[index].rect.left, "top": notesArray[index].rect.top, "width": notesArray[index].rect.width, "height": notesArray[index].rect.height });
        });
    }

    //Wywołanie funkcji wyświetlanie notatek z localstorage
    $.fn.displayLocalstorageNotes();

    // Zdarzenie wywoływane po klinięciu Add note w menu głownym
    $("#btn--add").click(function() {
        const idBase = Date.now();

        if( $("#btn--add").hasClass("ls--usage") ){
            $.fn.renderNote(idBase, "", notesArray.length);

            // Add created note to notesArray
            notesArray.push({id: idBase, content: "<p><br></p>", rect: {x: 50, y: 50, width: 202, height: 242, top: 50, right: 0, bottom: 0, left: 50}, color: "#ffffff"});
    
            // Update localstorage
            localStorage.setItem("notes", JSON.stringify(notesArray));
        } else {
            // Wywołanie funkcji tworzącej nową pustą notatkę
            let note = $.fn.renderNote(idBase, "");

            // Ustalenie domyślnych właściwości
            note.style.width = "200px";
            note.style.height = "240px";
            note.style.left = "15px";
            note.style.top = "0px";

            // Wysłanie żądania dodania nowego rekordu do bazy danych (pustej notatki)
            $.ajax({
                type: 'POST',
                url: 'http://127.0.0.1:3000/api/add_note',
                data: `id_note=${idBase}&id_user=${uid}&content=${""}&color=${"#ffffff"}&x=${note.getBoundingClientRect().x}&y=${note.getBoundingClientRect().y}&width=${note.getBoundingClientRect().width}&height=${note.getBoundingClientRect().height}&p_top=${note.getBoundingClientRect().top}&p_right=${note.getBoundingClientRect().right}&p_bottom=${note.getBoundingClientRect().bottom}&p_left=${note.getBoundingClientRect().left}`,
                success: () => {
                    console.log("Success on adding note into db!")
                }
            });
        }

    });

    // Manipulacja pozycją oraz wielkością notatek po zmianie rozmiaru okna przeglądarki
    function resizeStickers(){
        const stickerArray = Array.from($(".sticker"));
        let i = 1;
        let j = 1;

        stickerArray.forEach((element, index) => {
            if(parseInt(element.style.left) + parseInt(element.style.width) > this.innerWidth && this.innerWidth > 250) {
                element.style.left = 15 * i + "px";
                element.style.width = 200 + "px";
                i++

                let rect = element.getBoundingClientRect();

                if( $("#btn--add").hasClass("ls--usage") ){
                    // Aktualizuj Rect w notesArray
                    notesArray[index].rect = rect;

                    // Aktualizuj localstorage
                    localStorage.setItem("notes", JSON.stringify(notesArray));
                } else {
                    // Wysłanie żądania z updatem notatki po skończeniu manipulacją rozmiarem notatki
                    $.ajax({
                        type: 'PUT',
                        url: 'http://127.0.0.1:3000/api/update_note_rect',
                        data: `id_note=${element["id"].substr(1)}&x=${rect.x}&y=${rect.y}&width=${rect.width}&height=${rect.height}&p_top=${rect.top}&p_right=${rect.right}&p_bottom=${rect.bottom}&p_left=${rect.left}`,
                        success: () => { }
                    });
                }
            }

            if(parseInt(element.style.top) + parseInt(element.style.height) > this.innerHeight - 160 && this.innerHeight > 370) {
                element.style.top = 15 * j + "px";
                element.style.height = 240 + "px";
                j++

                let rect = element.getBoundingClientRect();

                if( $("#btn--add").hasClass("ls--usage") ){
                    // Aktualizuj Rect w notesArray
                    notesArray[index].rect = rect;

                    // Aktualizuj localstorage
                    localStorage.setItem("notes", JSON.stringify(notesArray));
                } else {
                    // Wysłanie żądania z updatem notatki po skończeniu manipulacją rozmiarem notatki
                    $.ajax({
                        type: 'PUT',
                        url: 'http://127.0.0.1:3000/api/update_note_rect',
                        data: `id_note=${element["id"].substr(1)}&x=${rect.x}&y=${rect.y}&width=${rect.width}&height=${rect.height}&p_top=${rect.top}&p_right=${rect.right}&p_bottom=${rect.bottom}&p_left=${rect.left}`,
                        success: () => { }
                    });
                }
            }
        });
    }

    // Trick, żeby event działał z lekkim opóźnieniem, żeby nie wysyłać zbyt dużo niepotrzebnych danych
    $(window).resize(function() {
        clearTimeout(doOnResize);
        doOnResize = setTimeout(function() {
            resizeStickers();
        }, 50);
    });

});