# Sticky Notes

"Sticky Notes" is team project for "Agiles Methodologies" classes. More important than the code in this case is to learn basics of SCRUM and get some experience in teamwork.

## About project
* **Team name** - MyszoJelenie
* **Team Capacity** - 4-6 hours / person
* **Length of sprint** - 2 weeks
* **Team Trello** - [sticky-notes](https://trello.com/b/myO9f9df/sticky-notes)

## About program

"Sticky Notes" - web application about to written in JavaScript. Thanks to MySQL user will be able to store, update and delete notes. Usage of Quill text editor will allow users to make notes more unique and advanced with simple options like 'bold' or 'italic'.

### First layout project

![First layout project](sticky_notes_layout_project.png)

### Database project

![Database project](sticky_notes_db_project.png)

## Getting Started

### Prerequisites

* [xampp](https://www.apachefriends.org)
* [node.js](https://nodejs.org/en/)

### Useful tools
* [Visual Studio Code](https://code.visualstudio.com/)

### Installing

Clone repository

Create hidden file `.gitignore`

Write inside "node_modules/" and save

Run server (for example XAMPP)

Import database from file `sticky_notes_database.sql` to MySQL

Change connection options in `server.js` file if needed

Init packages:

```
npm install
```

Install nodemon:
```
npm install nodemon
```

Run backend API - `server.js`:

```
nodemon server.js
```

Open `sticky_notes.html` file

## Technologies about to use

* [MySQL](https://www.mysql.com/) - basic database stucture
* [jQuery](https://jquery.com/) + JavaScript (ES5+)
* [jQuery UI](https://jqueryui.com/) - resizable and draggable notes
* [Quill](https://quilljs.com/) - rich text editor
* [Express](https://expressjs.com/) - backend
* [Font Awesome](https://fontawesome.com/) - some icons
* HTML
* CSS 3

## Authors

* **Kamil Adamus** - [Bitbucket](https://bitbucket.org/%7Ba906229b-3c13-4c76-956b-c450ae41027b%7D/)
* **Michał Kmieć** - [Bitbucket](https://bitbucket.org/%7B09563cee-772f-4d73-b790-797ca886e70e%7D/)
* **Kamila Kowalczyk** - [Bitbucket](https://bitbucket.org/%7Bf4f2a773-981c-439a-9c29-cd817d4de2be%7D/)

## License

This project is licensed under the MIT License