// Importy - to was nie interesuje
const express = require('express');
const mysql = require('mysql');
const cors = require('cors');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;

let url = require('url');

app.use(cors({origin: true, credentials: true}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Zmiana danych logowania do bazy, gdybyście mieli inne np hasło admina
let conn = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'sticky_notes'
});

// Łącznie się z bazą za pomocą powyższych danych logowania
conn.connect(error => {
    // W przypadku błedu wyrzuci go
    if (error) throw error;
    // Kiedy pomyślnie połączono z baza w konsoli wyskoczy stosowne info
    console.log('Connected with sticky-notes database!');
});

// Dalej zdefiniowane są endpointy, czyli ścieżki na które wysyła żądania klient w pliku sticky_notes.js
// Są w nich kwerenty mysql, trochę skomplikowane jest przekazywanie danych w poszczególnych metodach

/************** SELECTS **************/

// SELECT USER
app.post('/api/user_login', function(req, res) {
    let sql = `SELECT login, id_user FROM users WHERE login = ? AND password = ?`;
    conn.query(sql, [req.body.l, req.body.p], (error, results) => {
        if (error) throw error;

        return res.send(results);
    });
});

// SELECT ALL NOTES
app.post('/api/notes', function(req, res) {
    let sql = `SELECT * FROM notes WHERE id_user = ?`;
    conn.query(sql, [req.body.uid], (error, results) => {
        if (error) throw error;

        return res.send(results);
    });
});

/************** UPDATES **************/

app.put('/api/update_note_content', function(req, res) {
    let sql = `UPDATE notes SET content = ? , color = ? WHERE id_note = ?`;
    conn.query(sql, [req.body.content, req.body.color, req.body.id_note], (error, results) => {
        if (error) throw error;

        return res.send(results);
    });
});

app.put('/api/update_note_rect', function(req, res) {
    let sql = `UPDATE notes SET x = ? , y = ? , width = ? , height = ? , p_top = ? , p_right = ? , p_bottom = ? , p_left = ? WHERE id_note = ?`;
    conn.query(sql, [req.body.x, req.body.y, req.body.width, req.body.height, req.body.p_top, req.body.p_right, req.body.p_bottom, req.body.p_left, req.body.id_note], (error, results) => {
        if (error) throw error;

        return res.send(results);
    });
});

/************** INSERTS **************/

// ADD USER
app.post('/api/user_register', function(req, res) {
    let sql = `INSERT INTO users (login, password) values(?, ?)`;
    conn.query(sql, [req.body.l, req.body.p], (error, results) => {
        if (error) throw error;

        return res.send(results);
    });
});

// ADD NOTE
app.post('/api/add_note', function(req, res) {
    let sql = `INSERT INTO notes (id_note, id_user, content, color, x, y, width, height, p_top, p_right, p_bottom, p_left) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
    conn.query(sql, [req.body.id_note, req.body.id_user, req.body.content, req.body.color, req.body.x, req.body.y, req.body.width, req.body.height, req.body.p_top, req.body.p_right, req.body.p_bottom, req.body.p_left], (error, results) => {
        if (error) throw error;

        return res.send(results);
    });
});

/************** DELETES **************/
app.delete('/api/delete_note', function(req, res) {
    let sql = `DELETE FROM notes WHERE id_note = ?`;
    conn.query(sql, [req.body.id_note], (error, results) => {
        if (error) throw error;

        return res.send(results);
    });
});

// Nasłuchiwanie serwera na konkretnym porcie - port zdefiniowany jest u góry
app.listen(port, () => {
    console.log(`Listening to requests on port ${port}!`);
});